import * as React from "react";

import styled from "styled-components";

const StyledForm = styled.form`
  input {
    width: 100%;
    font-size: 1.2rem;
    padding: 0.5rem 0.5rem;
    box-sizing: border-box;
    border: none;
    border-bottom: 1px solid ${p => p.theme.color.text03};

    &:focus {
      border-bottom: 1px solid ${p => p.theme.color.focus};
      outline: 0;
    }

    &:focus::placeholder {
      color: ${p => p.theme.color.focus};
    }

    &::placeholder {
      color: ${p => p.theme.color.text03};
      font-weight: 100;
    }
  }
`;

export default class ItemForm extends React.Component<IItemFormProps, IItemFormState> {
  constructor(props: IItemFormProps) {
    super(props);
    this.state = {
      value: "",
    };
  }

  public render() {
    return (
      <StyledForm onSubmit={this.handleSubmit}>
        <input
          type="text"
          placeholder="Introduce una tarea"
          value={this.state.value}
          onChange={e => this.setState({ value: e.target.value })}
        />
      </StyledForm>
    );
  }

  private handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (!this.state.value) {
      return;
    }
    this.props.addItem(this.state.value);
    this.setState({ value: "" });
    return false;
  };
}

interface IItemFormProps {
  addItem(content: string): void;
}

interface IItemFormState {
  value: string;
}
