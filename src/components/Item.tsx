import * as React from "react";

import styled from "styled-components";

const StyledItem = styled.li<IStyledItem>`
  font-size: 1.2rem;
  padding: 1rem 1.5rem 1rem 0.5rem;
  border-bottom: 1px solid ${p => p.theme.color.ui03};
  cursor: pointer;
  position: relative;

  ${p =>
    p.isCompleted &&
    `
    color: ${p.theme.color.text03};
    text-decoration: line-through;
  `}
`;

const ItemName = styled.div``;

const Button = styled.button`
  position: absolute;
  top: 50%;
  right: 0;
  transform: translateY(-50%);
  cursor: pointer;

  &:hover {
    background-color: ${p => p.theme.color.focus};
    color: ${p => p.theme.color.hoverPrimary};
  }
`;

export default class Item extends React.Component<IItemProps> {
  public render() {
    return (
      <StyledItem isCompleted={this.props.isCompleted}>
        <ItemName onClick={this.onClickHandler}>{this.props.content}</ItemName>
        <Button onClick={this.buttonHandler}>X</Button>
      </StyledItem>
    );
  }

  private onClickHandler = () => {
    if (this.props.completeItem && this.props.index !== undefined) {
      return this.props.completeItem(this.props.index);
    }
  };

  private buttonHandler = () => {
    if (this.props.deleteItem && this.props.index !== undefined) {
      return this.props.deleteItem(this.props.index);
    }
  };
}

export interface IItemProps {
  index?: number;
  content: string;
  isCompleted: boolean;
  completeItem?(index: number): void;
  deleteItem?(index: number): void;
}

interface IStyledItem {
  isCompleted: boolean;
}
