import _theme from "./theme";
import custom from "./custom";
import { parseTheme } from "@sqymagma/theme";

const theme = {
  ...parseTheme(_theme),
  ...custom,
};

export default theme;
