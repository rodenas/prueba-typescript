// ---
// Magma by Secuoyas. 2018.
// Global CSS theme file.
// ---

import { css } from "styled-components"
const Global = css`
  html {
    box-sizing: border-box;
  }

  body {
    font-family: ${({ theme }) => theme.fontFamily.primary};
    background-color: ${({ theme }) => theme.color.ui02};
    color: ${({ theme }) => theme.color.text01};
  }

  strong,
  b {
    font-weight: bold;
  }

  ol,
  ul,
  li {
    margin: 0;
    padding: 0;
  }

  li {
    list-style: none;
  }

  * {
    box-sizing: border-box;
  }

  img {
    display: block;
    width: 100%;
  }

  a {
    text-decoration: none;
  }
`
export default Global
