import * as React from "react";

import Item, { IItemProps } from "./components/Item";
import ItemForm from "./components/ItemForm";

import styled, { createGlobalStyle, ThemeProvider } from "styled-components";

import darkTheme from "./themes/dark";
import theme from "./themes/default";

const GlobalStyle = createGlobalStyle`
  @import url("https://fonts.googleapis.com/css?family=Open+Sans:300,400&display=swap");

  body {
    margin: 0;
    background: ${(p: any) => p.theme.color.primaryBackground};
    display: flex;
    align-items: center;
    justify-content: center;
    min-height: 100vh;
    font-family: "Open Sans", sans-serif;
  }

  #root {
    height: fit-content;
    width: fit-content;
  }
`;

const Wrapper = styled.div`
  width: 20rem;
  max-width: 90vw;
  height: 30rem;
  max-height: 90vh;
  margin: 1rem;
  background: ${p => p.theme.color.ui01};
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0px 1px 2px 0px rgba(50, 50, 50, 0.2);
  color: ${p => p.theme.color.text01};
  display: flex;
  flex-direction: column;
`;

const Title = styled.h1`
  margin: 0;
  margin-bottom: 1.5rem;
  font-weight: 200;
  font-size: 3rem;
  position: relative;
  color: ${p => p.theme.color.text05};
  text-align: center;
`;

const List = styled.ul`
  margin: 1rem 0;
  list-style-type: none;
  padding: 0;
  overflow: auto;
`;

const defaultItems = [
  {
    content: "📘 Aprender React",
    isCompleted: false,
  },
  {
    content: "⚛️ Crear mi primera aplicación",
    isCompleted: false,
  },
  {
    content: "🚀 Subirla a GitHub",
    isCompleted: false,
  },
];

export default class App extends React.Component<{}, IAppState> {
  constructor(props: any) {
    super(props);
    this.state = {
      darkTheme: false,
      items: defaultItems,
    };
  }

  public render() {
    return (
      <ThemeProvider theme={this.state.darkTheme ? darkTheme : theme}>
        <>
          <GlobalStyle />
          <Wrapper>
            <Title>ToDo List</Title>
            <ItemForm addItem={this.addItem} />
            <List>
              {this.state.items.map((item, index) => (
                <Item
                  key={index}
                  index={index}
                  content={item.content}
                  isCompleted={item.isCompleted}
                  completeItem={this.completeItem}
                  deleteItem={this.deleteItem}
                />
              ))}
            </List>
          </Wrapper>
        </>
      </ThemeProvider>
    );
  }

  private addItem = (content: string) => {
    const newItems = [...this.state.items];
    newItems.unshift({ content, isCompleted: false });
    this.setState({ items: newItems });
  };

  private completeItem = (index: number) => {
    const newItems = [...this.state.items];
    newItems[index].isCompleted = !newItems[index].isCompleted;
    this.setState({ items: newItems });
  };

  private deleteItem = (index: number) => {
    const newItems = [...this.state.items];
    newItems.splice(index, 1);
    this.setState({ items: newItems });
  };

  private changeTheme = () => {
    let dark = this.state.darkTheme;
    dark = !dark;
    this.setState({ darkTheme: dark });
  };
}

interface IAppState {
  darkTheme: boolean;
  items: IItemProps[];
}
